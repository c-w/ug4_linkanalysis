\documentclass[a4paper,twocolumn,twoside]{article}

\usepackage{hyperref}
\usepackage{titling}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{appendix}
\usepackage{amsmath}
\usepackage[margin=0.90in]{geometry}

\setlength{\droptitle}{-4em}
\date{\vspace{0em}}
\pagenumbering{gobble}

\newcommand*{\eg}{e.g.\xspace}
\newcommand*{\ie}{i.e.\xspace}
\newcommand*{\In}{\ensuremath{\mathit{In}}\xspace}
\newcommand*{\Out}{\ensuremath{\mathit{Out}}\xspace}
\newcommand*{\Hub}{\ensuremath{\mathit{Hub}}\xspace}
\newcommand*{\Auth}{\ensuremath{\mathit{Auth}}\xspace}
\newcommand*{\HITS}{\ensuremath{\mathit{HITS}}\xspace}
\newcommand*{\PageRank}{\ensuremath{\mathit{PageRank}}\xspace}
\newcommand*{\PR}{\ensuremath{\mathit{PR}}\xspace}

\DeclareMathOperator{\argmax}{arg\,max}

\renewcommand\appendix{\par
  \setcounter{section}{0}
  \setcounter{subsection}{0}
  \setcounter{figure}{0}
  \setcounter{table}{0}
  \renewcommand\thesection{Appendix \Alph{section}}
  \renewcommand\thefigure{\Alph{section}\arabic{figure}}
  \renewcommand\thetable{\Alph{section}\arabic{table}}}

% Prepare a 2-page report. For task 1 briefly outline the major implementation
% decisions you made and discuss any interesting observations. For task 2, state
% precisely what techniques you used to select the key persons and to decide
% which links to include in the visualization. Include the graph visualization
% in your report (the graph can be on a separate page, and does count toward the
% 2- page limit). Discuss any interesting observations and take-away lessons.

\title{Text Technologies - Assessment 4}
\author{Clemens Wolff (s0942284)}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report investigates a large corpus of email communications within the Enron
corporation and proposes an automated way to find the answer to the question
``who talks to whom and about what''.
Section~\ref{sec:graph} shows how we can model the corpus as a graph of emails
and senders or recipients and how this enables us to explore the corpus using
link analysis techniques. Section~\ref{sec:visualization} shows how we can
automatically find interesting parts of the graph and visualise them.
Section~\ref{sec:conclusion} synthesises the findings and concludes the report.

\section{Algorithms on the graph}\label{sec:graph}

A natural way to model relations between entities is a graph: each vertex models
an entity, each edge models a relation between two entities. In our case, we
can model the corpus of email communications as the graph $G = (V, E)$ where the
vertex-set $V$ contains a node for every individuals involved in communications
with Enron and the edge set $E$ contains an edge $e_{a,b} = v_a \rightarrow v_b$
if person $a$ sent an email to person $b$. For any person $p$ we can then answer
the following questions: from whom does $p$ receive emails and whom does $p$
send emails to -- or equivalently: what are the nodes $\In_p = \left\{ x \mid
e_{x,p} \in E\right\}$ and what are the nodes $\Out_p = \left\{y \mid e_{p,y}
\in E\right\}$.

Note how the size of $\Out_p$ (from here onwards: ``out-degree'') and the size
of $\In_p$ (from here onwards: ``in-degree'') already give us a crude way of
judging the importance of person $p$. In Section~\ref{subsec:algos} we will
refine this metric by also taking into consideration the in- and out-degrees of
the $p'$ in $\Out_p$ and $\In_p$, the in- and out-degrees of the $p''$ in
$\Out_{p'}$ and $\In_{p'}$, and so forth.

\subsection{Graph implementation}\label{subsec:impl}

Having convinced ourselves that a graph is a suitable way to model our problem,
we move on to implementation. Traditionally, graphs are based on either an
adjacency matrix $M_{ij} = \left\|\left\{e_{i,j} \in E\right\}\right\|$ or on an
adjacency list $A_i = [e_{i,j} \mid j \in \Out_i]$. The adjacency matrix
representation is not recommendable since our graph has about 85,000 nodes which
at 16 bits for an integer would require about 14 gigabytes of memory. Similarly,
the standard adjacency list representation is also not well suited to the
problem: while we can find $\Out_p$ in constant time, it takes us time
proportional to $O(\left\|E\right\|) \times O(\left\|V\right\|)$ to find
$\In_p$. Less abstractly: finding $\In_p$ for a single node $p$ takes about 0.06
seconds in our Python adjacency-list-graph implementation using dictionaries --
or about $85,000 \times 0.06 \approx 1.5 $ hours to find $\In_p$ for every $p$
in $V$.

Clearly both traditional graph data-structures are not suitable for our problem.
However if we notice that the adjacency-list based graph had constant time
look-up for $\In_p$, we find a solution: store $A^T_i = [e_{j,i} \mid j \in
\In_i]$ in addition to $A$. We can then use $A^T$ to find $\In_i$ in $O(1)$ time
and $A$ to find $\Out_i$ in $O(1)$ time. In our Python implementation, this
reduces the time to find $\In_p$ and $\Out_p$ for every $p$ in $V$ to 1.2
minutes, inclusive of the time required to transpose $A$ to get $A^T$.

\subsection{Graph analysis}\label{subsec:algos}

Next, we come back to the idea of using $\Out_p$ and $\In_p$ recursively in
order to compute the importance of node $p$.

The \HITS algorithm \cite{hits} is a natural way to formalise our idea. \HITS
analyses every node $x$ in the graph along two dimensions: $x$ has a high
``hub'' score \Hub if many nodes with high ``authority'' score \Auth point
towards it, and similarly, $x$ has a high ``authority'' score if many nodes with
a high ``hubs'' score point to it:

\begin{equation}\label{eq:hub}
\Hub(x) = \sum\limits_{y \rightarrow x} \Auth(y),
\end{equation}

\begin{equation}\label{eq:auth}
\Auth(x) = \sum\limits_{x \rightarrow y} \Hub(y).
\end{equation}

Appendix~\ref{tbl:hits} gives the \Hub and \Auth scores for the ten highest
ranked nodes in our graph.

\HITS can be seen as a pre-cursor to the more widely known \PageRank algorithm
\cite{pagerank} which eschews \HITS' \Hub component for a more sophisticated way
of computing a node's authoritativeness. The \PageRank score \PR of a node $x$
is the probability that a random walker on the graph is at node $x$ at any point
in time:

\begin{equation}\label{eq:pagerank}
\PR(x) =
{1 - \lambda \over \left\|V\right\|}
+ \lambda \sum\limits_{y \in \In_x}
    {\PR(y) \over \left\|\Out_y\right\|}
+ \lambda \sum\limits_{z \not\in \In_k \forall k \in V}
    {\PR(z) \over \left\|V\right\|}.
\end{equation}

Appendix~\ref{tbl:pagerank} gives the \PageRank score for the ten highest rated
nodes in our graph.

\section{Data visualisation}\label{sec:visualization}

Equations~\ref{eq:hub},~\ref{eq:auth} and~\ref{eq:pagerank} give us metrics to
analyse the importance of any \emph{individual} node $x$ along the $\In_x$ and
$\Out_x$ dimensions. This section proposes a three-step process to produce a
visualisation of a \emph{subset} of nodes that is interesting under these
metrics: first we extract a salient sub-graph (Section~\ref{subsec:subgraph},
then we annotate the edges automatically (Section~\ref{subsec:labels}) and
lastly we perform some manual cleanup.\footnote{Cleanup steps include using
limited domain knowledge to remove edges with less interesting labels and
removing labels from the edge $e_{i,j}$ if $e_{j,i}$ has the same labels. A
non-post-processed version of the graph is available at
\url{http://homepages.inf.ed.ac.uk/s0942284/tts4-graph.png}.}

\subsection{Sub-graph extraction}\label{subsec:subgraph}

A $N$-node sub-graph $S = (V_S, E_S)$ of the corpus' email-graph $G$ is
extracted as follows.

\begin{enumerate}
    \item First, let $V_\Hub$ be $V$ sorted according to \Hub scores.

    \item Add node $x$ with the highest \Hub score to the set $S_\Hub$ and add
    the node with the highest \Auth score in $\In_x$ to the set $S_\Auth$.

    \item Find the nodes
    \begin{equation*}
    h = \argmax\Hub(h),~h \in \left\{\In_y \mid y \in S_\Auth\right\},
    \end{equation*}
    \begin{equation*}
    a = \argmax\Auth(a),~a \in \left\{\Out_y \mid y \in S_\Hub\right\},
    \end{equation*}
    that are not already in either of $S_\Hub$ or $S_\Auth$.

    \item If no $a$ or $h$ were found, continue with Step 2.

    \item If $\Auth(a) > \Hub(h)$, add $a$ to $S_\Auth$, otherwise, add $h$ to
    $S_\Hub$.

    \item Repeat with Step 3 until $\left\|S_\Hub\right\| +
    \left\|S_\Auth\right\| = N$.

    \item Let $V_S = S_\Hub \cup S_\Auth$ and let $E_S$ be all the edges in $E$
    that have both endpoints in $V_S$.
\end{enumerate}

Intuitively, this method builds the maximum \Auth- and \Hub-weight $N$-node
sub-graph starting at $x$. Empirically, the method gave more interesting results
than other methods such as constructing a maximum node weight tree (weights
being \PR or \Auth or \Hub) starting at $x$.

A notable implementation detail is that we should take care to remove the node
with the very highest \Hub score, ``Pete Davis'', from $V_\Hub$ before running
the algorithm: the node is not a real person but an automated ``schedule
crawler''. This also explains that node's incredibly high \Hub score: ``Pete
Davis'' will have replied (\ie sent an email -- collected an edge) to anyone who
has used its schedule crawling service, thus accumulating the \Auth scores of
the majority of the corporation for its \Hub score.

\subsection{Meta-data generation}\label{subsec:labels}

The procedure outlined in Section~\ref{subsec:subgraph} utilises \Hub and \Auth
scores to generate an interesting sub-graph $S$ from our email corpus. Note that
the procedure is not utilising the third score we have compute (\PR) or any
domain knowledge (\eg the content of the emails in the corpus). This section
proposes a way to visualise $S$ that addresses both of these shortcomings.

First, we set the size of any node $x$ in the visualization of $V_S$
proportional to $\log\PR(x)$.

Next, we train a Parsimonious language model\footnote{Using the library in
\cite{py-parsimonious}.} on the full text of the emails represented by the edges
in $E_S$. We now label each edge $e \in E_S$ by asking the language model to
produce the 4 most surprising words in the email associated with the edge $e$
(after removing stop-words and names of employees in the corporation).
Empirically, this approach produces more salient labels than extracting
point-wise-mutual-information collocations \cite{pmi-collocations} or using the
RAKE algorithm \cite{rake}.

\section{Conclusion}\label{sec:conclusion}

Section~\ref{sec:graph} proposed two ``interest'' metrics for graphs: \HITS and
\PageRank. Section~\ref{sec:visualization} used these metrics to find an
interesting subset in a corpus of email communications between people involved
with the Enron corporation. The Section also proposed an automated way to label
the edges in the sub-graph with keywords extracted from the bodies of the
emails. The resulting graph can be found in Appendix~\ref{fig:subgraph}.

\begin{thebibliography}{9}

\bibitem{hits}
    J. Kleinberg,
    \emph{Authoritative sources in a hyperlinked environment},
    Journal of the ACM,
    1999

\bibitem{pagerank}
    S. Brin, L. Page,
    \emph{The anatomy of a large-scale hypertextual Web search engine},
    Computer Networks and ISDN Systems,
    1998

\bibitem{pmi-collocations}
    B. Das, P. Subhajit, S. Kr. Mondal, D. Dalui, S. K. Shome,
    \emph{Automatic Keyword Extraction From Any Text Document Using N-gram Rigid
    Collocation},
    International Journal of Soft Computing \& Engineering,
    2013

\bibitem{rake}
    M. W. Berry, J. Kogan,
    \emph{Text Mining: Applications and Theory},
    Wiley,
    2010

\bibitem{py-parsimonious}
    L. Buitnick,
    \emph{WeighWords},
    The University of Amsterdam,
    2011,
    available on-line at \url{https://github.com/larsmans/weighwords}
\end{thebibliography}

\onecolumn\appendix

\section{\HITS and \PageRank scores}\label{app:scores}

\begin{table}[ht]
    \centering
    \begin{tabular}{c l | c l}
        \Auth & Node & \Hub & Node \\
        \hline
        0.3842 & Ryan Slinger &  0.9993 & Pete Davis \\
        0.3842 & Albert Meyers & 0.0330 & Bill Williams \\
        0.3838 & Mark Guzman &   0.0104 & Rhonda Denton \\
        0.3838 & Geir Solberg &  0.0068 & L Denton \\
        0.3556 & Craig Dean &    0.0058 & Grace Rodriguez \\
        0.2779 & Bill Williams & 0.0048 & Alan Comnes \\
        0.2158 & John Anderson & 0.0045 & Kathryn Sheppard \\
        0.2157 & Michael Mier &  0.0040 & Kate Symes \\
        0.1722 & Leaf Harasin &  0.0038 & Kysa Alport \\
        0.1432 & Eric Linder &   0.0028 & Carla Hoffman \\
    \end{tabular}
    \caption{Top 10 \HITS scores}
    \label{tbl:hits}
\end{table}

\begin{table}[ht]
    \centering
    \begin{tabular}{c l}
        \PageRank & Node \\
        \hline
        0.0080 & Klay \\
        0.0030 & Jeff Skilling \\
        0.0030 & Sara Shackleton \\
        0.0028 & Tana Jones \\
        0.0027 & Mark Taylor \\
        0.0027 & Kenneth Lay \\
        0.0024 & Louise Kitchen \\
        0.0022 & Gerald Nemec \\
        0.0021 & Jeff Dasovich \\
        0.0018 & Sally Beck \\
    \end{tabular}
    \caption{Top 10 \PageRank scores}
    \label{tbl:pagerank}
\end{table}

\section{Salient sub-graph visualisation}\label{app:subgraph}

\begin{figure}[t]
    \centering
    \includegraphics[width=\columnwidth]{../data/subgraph.png}
    \caption{Visualisation of sub-graph $S$}
    \label{fig:subgraph}
\end{figure}


\end{document}
