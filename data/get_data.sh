#!/bin/bash

set -e

graph="http://ir.inf.ed.ac.uk/tts/A4/graph.txt"
roles="http://www.inf.ed.ac.uk/teaching/courses/tts/assessed/roles.txt"
mails="http://ir.inf.ed.ac.uk/tts/A4/enron.xml.bz2"

graph_file=$(basename $graph .txt)
roles_file=$(basename $roles .txt)
mails_file=$(basename $mails .xml.bz2)

wget $graph
wget $roles
wget $mails && bunzip2 $mails_file.xml.bz2

cat $roles_file.txt | sed 's/\(\t\+\)\|  \+/,/g' > $roles_file.csv
rm $roles_file.txt

echo $graph_file* >> .gitignore
echo $roles_file* >> .gitignore
echo $mails_file* >> .gitignore
