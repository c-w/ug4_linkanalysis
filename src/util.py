import nltk


def mean(nums):
    nums = list(nums)
    return sum(nums) / float(len(nums))


def sdev(nums):
    mu = mean(nums)
    return (mean(x ** 2 for x in nums) - mu ** 2) ** 0.5


def normalize_dict(d):
    mu = mean(d.values())
    sigma = sdev(d.values())
    return dict((k, (v - mu) / float(sigma))
                for (k, v) in d.iteritems())


def tokenize(text, stops=set()):
    sents = nltk.sent_tokenize(text.lower())
    toks = [w for s in sents for w in nltk.word_tokenize(s)]
    words = [w for w in toks
             if w not in tokenize._stops
             and w not in tokenize._namestops
             and w not in stops]
    return [w.lower() for w in words]
tokenize._stops = set(w.lower() for w in nltk.corpus.stopwords.words())
tokenize._namestops = set(w.lower() for w in nltk.corpus.names.words())
