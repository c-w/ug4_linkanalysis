import abc
import collections
import heapq
import numpy


def dict_to_matrix(graph, dtype=numpy.uint16):
    start_nodes = set(graph.iterkeys())
    end_nodes = set(node for links in graph.itervalues() for node in links)
    nodes = sorted(set.union(start_nodes, end_nodes))
    node_to_idx = dict((n, i) for (i, n) in enumerate(nodes))
    idx_to_node = dict((i, n) for (n, i) in node_to_idx.iteritems())
    matrix = numpy.zeros((len(nodes), len(nodes)), dtype=dtype)
    for A, links in graph.iteritems():
        for B in links:
            matrix[node_to_idx[A]][node_to_idx[B]] += 1
    return matrix, node_to_idx, idx_to_node


def transpose_dict(graph):
    d = collections.defaultdict(list)
    for A, links in graph.iteritems():
        for B in links:
            d[B].append(A)
    return d


Node = collections.namedtuple('Node', 'payload parent')


class Graph(object):
    __metaclass__ = abc.ABCMeta

    @staticmethod
    def from_adjacency_list(adjacency_dict, is_sparse=True):
        return (AdjacencyListGraph(adjacency_dict) if is_sparse else
                AdjacencyMatrixGraph(*dict_to_matrix(adjacency_dict)))

    @abc.abstractmethod
    def nodes(self):
        pass

    @abc.abstractmethod
    def remove_node(self, A):
        pass

    @abc.abstractmethod
    def incoming_neighbours(self, A):
        pass

    @abc.abstractmethod
    def outgoing_neighbours(self, A):
        pass

    def in_degree(self, A):
        return len(self.incoming_neighbours(A))

    def out_degree(self, A):
        return len(self.outgoing_neighbours(A))

    def max_node_weight_tree(self, A, weights, depth=-1):
        Node.__lt__ = lambda a, b: weights[a.payload] > weights[b.payload]
        out_nodes = self.outgoing_neighbours
        result = [Node(A, None)]
        visited = set(A)
        frontier = [Node(x, A) for x in out_nodes(A) if x not in visited]
        heapq.heapify(frontier)
        while depth < 0 or len(result) < depth:
            while True:
                try:
                    B = heapq.heappop(frontier)
                    if B.payload not in visited:
                        break
                except IndexError:
                    return result
            result.append(B)
            visited.add(B.payload)
            for x in (x for x in out_nodes(B.payload) if x not in visited):
                heapq.heappush(frontier, Node(x, B.payload))
        return result

    def hubs_and_authorities(self, num_iterations):
        incoming_neighbours = self.incoming_neighbours
        outgoing_neighbours = self.outgoing_neighbours
        G = self.nodes()
        N = len(G)
        auth = dict((n, N ** -0.5) for n in G)
        hub = dict((n, N ** -0.5) for n in G)
        for _ in xrange(num_iterations):
            # an authority is a node that is pointed to by good hubs
            for p in G:
                auth[p] = sum(hub[q] for q in incoming_neighbours(p))
            # a hub is a node that is pointed to by good authorities
            for p in G:
                hub[p] = sum(auth[r] for r in outgoing_neighbours(p))
            # normalize to guarantee eventual convergence
            hub_norm = sum(hub[p] ** 2 for p in G) ** 0.5
            for p in G:
                hub[p] /= hub_norm
            auth_norm = sum(auth[p] ** 2 for p in G) ** 0.5
            for p in G:
                auth[p] /= auth_norm
        return hub, auth

    def page_rank(self, d, num_iterations):
        out_degree = self.out_degree
        in_nodes = self.incoming_neighbours
        pages = self.nodes()
        sinks = set(p for p in pages if out_degree(p) == 0)
        N = len(pages)
        pr = dict((p, 1.0 / N) for p in pages)
        pr_new = dict(pr)
        for _ in xrange(num_iterations):
            sink_pr = sum(pr[s] for s in sinks)
            for p in pages:
                # jump to a random node
                rank = (1.0 - d) / N
                # spread page-rank of sinks evenly
                rank += d * sink_pr / N
                # add a share of page-rank from incoming links
                rank += d * sum(pr[q] / out_degree(q) for q in in_nodes(p))
                pr_new[p] = rank
            pr = dict(pr_new)
        return pr


class AdjacencyMatrixGraph(Graph):
    def __init__(self, adjacency_matrix, node_to_idx=None, idx_to_node=None):
        N = len(adjacency_matrix)
        self._matrix = adjacency_matrix
        self._n2i = node_to_idx or dict((i, i) for i in xrange(N))
        self._i2n = idx_to_node or dict((i, i) for i in xrange(N))

    def nodes(self):
        return self._n2i.keys()

    def remove_node(self, A):
        raise NotImplementedError

    def incoming_neighbours(self, A):
        try:
            return self._incoming[A]
        except AttributeError:
            self._incoming = {}
        except KeyError:
            pass
        n2i = self._n2i
        i2n = self._i2n
        mat = self._matrix
        in_nodes = [i2n[i] for (i, B) in enumerate(mat[:, n2i[A]])
                    for _ in xrange(B) if B > 0]
        self._incoming[A] = in_nodes
        return in_nodes

    def outgoing_neighbours(self, A):
        try:
            return self._outgoing[A]
        except AttributeError:
            self._outgoing = {}
        except KeyError:
            pass
        n2i = self._n2i
        i2n = self._i2n
        mat = self._matrix
        out_nodes = [i2n[i] for (i, B) in enumerate(mat[n2i[A], :])
                     for _ in xrange(B) if B > 0]
        self._outgoing[A] = out_nodes
        return out_nodes


class AdjacencyListGraph(Graph):
    def __init__(self, adjacency_dict):
        self._graph = adjacency_dict
        self._graph_transpose = transpose_dict(adjacency_dict)

    def nodes(self):
        starting = set(self._graph.iterkeys())
        ending = set(self._graph_transpose.iterkeys())
        return set.union(starting, ending)

    def remove_node(self, A):
        del self._graph[A]
        self._graph_transpose = transpose_dict(self._graph)

    def incoming_neighbours(self, A):
        return self._graph_transpose.get(A, [])

    def outgoing_neighbours(self, A):
        return self._graph.get(A, [])
