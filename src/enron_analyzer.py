#!/usr/bin/env python


import graph
import math
import model
import os
import os.path
import re
import util


def main(graph_path, roles_path, emails_path, names_path,
         hubs_path, auth_path, pr_path, plot_path):
    email_graph, email_idents = model.EmailGraph.from_file(graph_path)
    write_page_rank_and_hits(email_graph, pr_path, hubs_path, auth_path)
    visualize_key_connections(email_graph, email_idents, plot_path,
                              names_path, roles_path, emails_path)


def visualize_key_connections(email_graph, email_idents, plot_path,
                              names_path, roles_path, emails_path):
    # remove junk node and re-compute graph importance metricd
    email_graph.remove_node('pete.davis@enron.com')
    pr_score = email_graph.page_rank(d=0.8, num_iterations=10)
    hub_score, auth_score = email_graph.hubs_and_authorities(num_iterations=10)
    # find an interesting subgraph
    nodes = hits_subgraph(email_graph, hub_score, auth_score, size=10)
    # extract meta-info and plot result
    roles = dict((r.email, r) for r in model.read_roles_from_file(roles_path))
    labels = generate_labels(nodes, emails_path, names_path, email_idents)
    _write_nodes(plot_path, nodes, sizes=pr_score, roles=roles, labels=labels)


def write_page_rank_and_hits(email_graph, pr_path, hubs_path, auth_path):
    # compute graph metrics
    pr_score = email_graph.page_rank(d=0.8, num_iterations=10)
    hub_score, auth_score = email_graph.hubs_and_authorities(num_iterations=10)
    # write results
    _write_ranks(pr_path, pr_score)
    _write_ranks(hubs_path, hub_score)
    _write_ranks(auth_path, auth_score)


def generate_labels(nodes, email_fulltext_path, names_path, email_idents):
    stops = _stopwords(names_path)
    email_text = _extract_email_text(email_fulltext_path)
    docs = _tokenize_text_for_nodes(nodes, email_text, email_idents, stops)
    language_model = model.ParsimoniousLM(docs, w=0.01, thresh=1)
    labels = {}
    for (i, node) in enumerate(nodes):
        document = docs[i]
        if not document:
            continue
        keywords = [x[0] for x in language_model.top(4, document)]
        labels[node.parent, node.payload] = keywords
        labels[node.payload, node.parent] = keywords
    return labels


def _stopwords(names_path):
    stops = set(['thanks', 'thank', 'www', 'http', 'https', 'tel', 'group',
                 'iso', 'inc', 'cd', 'please', 'dude', 'anyway', 'hey', 'much',
                 'username', 'password', 'fax', 'guy', 'bright', 'gngr',
                 'okay', 'follows', 'yeah', 'phone', 'cdt', 'cst', 'location',
                 'god', 'hotel', 'asked', 'passcode'])
    stops.update(w.strip().lower() for w in open(names_path))
    return stops


def _tokenize_text_for_nodes(nodes, email_text, email_idents, stops):
    documents = []
    for node in nodes:
        relevant_emails = email_idents[node.parent][node.payload]
        relevant_emails += email_idents[node.payload][node.parent]
        fulltext = '\n'.join(email_text.get(ident, '')
                             for ident in relevant_emails)
        documents.append(util.tokenize(fulltext, stops))
    return documents


def _extract_email_text(path, message_delim='DOC', ident_delim='DOCID'):
    with open(path) as f:
        text = '\n'.join(l.strip() for l in f)
    delim_re = re.compile('<%s>' % message_delim, re.MULTILINE | re.IGNORECASE)
    ident_re = re.compile('<%s>([^<]*)</%s>' % (ident_delim, ident_delim),
                          re.MULTILINE | re.IGNORECASE)
    content_re = re.compile('X-FileName:[^\n]*\n',
                            re.MULTILINE | re.IGNORECASE)
    chunks = [x[:-(len(message_delim) + 4)] for x in delim_re.split(text) if x]
    sentences = {}
    for chunk in chunks:
        try:
            ident = ident_re.search(chunk).groups()[0].strip().split('/')[-1]
            content = chunk[content_re.search(chunk).end():].strip()
            try:
                content = content[:content.index('----')]
            except ValueError:
                pass
            words = re.findall('\w+', content, re.MULTILINE)
            content = ' '.join(w for w in words if w.isalpha())
            sentences[ident] = content
        except:
            continue
    return sentences


def _email_to_title(s):
    root = s.split('@')[0]
    try:
        first, last = root.split('.')
        return ('%s %s'
                % (first[0].upper() + first[1:], last[0].upper() + last[1:]))
    except ValueError:
        return root[0].upper() + root[1:]


def hits_subgraph(email_graph, hub_score, auth_score, size,
                  excludes=set(['pete.davis@enron.com']), expand=True):
    hub_score = util.normalize_dict(hub_score)
    auth_score = util.normalize_dict(auth_score)
    seeds = iter(sorted(hub_score, key=hub_score.get, reverse=True))
    maxval_auths = []
    maxval_hubs = []
    visited = set()

    def largest_hub(node):
        in_nodes = email_graph.incoming_neighbours(node)
        in_nodes = (x for x in in_nodes
                    if x not in visited and x not in excludes)
        return max(in_nodes, key=hub_score.get)

    def highest_auth(node):
        out_nodes = email_graph.outgoing_neighbours(node)
        out_nodes = (x for x in out_nodes
                     if x not in visited and x not in excludes)
        return max(out_nodes, key=auth_score.get)

    def get_auth_candidates():
        for existing_hub in (n.payload for n in maxval_hubs):
            try:
                yield graph.Node(highest_auth(existing_hub), existing_hub)
            except ValueError:
                pass

    def get_hub_candidates():
        for existing_auth in (n.payload for n in maxval_auths):
            try:
                yield graph.Node(largest_hub(existing_auth), existing_auth)
            except ValueError:
                pass

    def push_hub(node):
        maxval_hubs.append(node)
        visited.add(node.payload)
        visited.add(node.parent)

    def push_auth(node):
        maxval_auths.append(node)
        visited.add(node.payload)
        visited.add(node.parent)

    def push_seed():
        seed = next(seeds)
        while seed in excludes:
            seed = next(seeds)
        push_hub(graph.Node(seed, None))
        push_auth(graph.Node(highest_auth(seed), seed))

    push_seed()
    while len(visited) < size + 1:
        auth_candidates = list(get_auth_candidates())
        hub_candidates = list(get_hub_candidates())
        if len(auth_candidates) > 0 and len(hub_candidates) > 0:
            auth = max(auth_candidates)
            hub = max(hub_candidates)
            if auth_score[auth.payload] > hub_score[hub.payload]:
                push_auth(auth)
            else:
                push_hub(hub)
        elif len(auth_candidates) > 0:
            push_auth(max(auth_candidates))
        elif len(hub_candidates) > 0:
            push_hub(max(hub_candidates))
        else:
            push_seed()
    if not expand:
        return auth_candidates + hub_candidates
    maxgraph = set()
    for a in visited:
        a_out = set(email_graph.outgoing_neighbours(a))
        a_in = set(email_graph.incoming_neighbours(a))
        for b in visited:
            b_out = set(email_graph.outgoing_neighbours(b))
            b_in = set(email_graph.incoming_neighbours(b))
            ab = graph.Node(a, b)
            ba = graph.Node(b, a)
            if ba not in maxgraph and (a in b_out or b in a_in):
                maxgraph.add(graph.Node(b, a))
            if ab not in maxgraph and (a in b_in or b in a_out):
                maxgraph.add(graph.Node(a, b))
    return list(maxgraph)


def _write_ranks(path, d):
    items = sorted(((v, k) for (k, v) in d.iteritems()), reverse=True)
    with open(path, 'w') as f:
        f.write('\n'.join('%.8f %s' % t for t in items))


def _write_nodes(path, nodes, roles, sizes, base_size=1.50, labels={}):
    # normalize sizes
    parents = set(n.parent for n in nodes)
    payloads = set(n.payload for n in nodes)
    sizes = dict((k, math.log(v + 1) / math.log(2))
                 for (k, v) in sizes.iteritems()
                 if k in parents or k in payloads)
    min_size = min(sizes.itervalues())
    max_size = max(sizes.itervalues())
    size_range = float(max_size - min_size)
    sizes = dict((k, base_size + base_size * (v - min_size) / size_range)
                 for (k, v) in sizes.iteritems())
    # find key connections
    path_root, _ = os.path.splitext(path)
    with open(path_root + '.dot', 'w') as f:
        f.write('digraph G {\n')
        f.write('concentrate=true;\n')
        f.write('node [shape=circle,fixedsize=true];\n')
        for (i, node) in enumerate(nodes):
            # write nodes
            payload = roles.get(node.payload) or _email_to_title(node.payload)
            parent = roles.get(node.parent) or _email_to_title(node.parent)
            f.write('"%s" [width=%s];\n' % (parent, sizes[node.payload]))
            f.write('"%s" [width=%s];\n' % (payload, sizes[node.parent]))
            # write edge
            label = labels.get((node.parent, node.payload))
            label = ' [label="%s"]' % ' '.join(label) if label else ''
            f.write('"%s" -> "%s"%s;\n' % (parent, payload, label))
        f.write('}')
    # convert to png
    os.system('dot -Tpng %s > %s' % (path_root + '.dot', path_root + '.png'))
    os.remove(path_root + '.dot')


if __name__ == '__main__':
    import optparse
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('--email-graph', metavar='E', dest='graph',
                      default='../data/graph.txt',
                      help='read email graph from E')
    parser.add_option('--corporate-roles', metavar='R', dest='roles',
                      default='../data/roles.csv',
                      help='read corporate roster from R')
    parser.add_option('--employee-names', metavar='N', dest='names',
                      default='../data/names.txt',
                      help='read employee names from N')
    parser.add_option('--email-fulltext', metavar='T', dest='fulltext',
                      default='../data/enron.xml',
                      help='read email text from T')
    parser.add_option('--page-rank', metavar='P', dest='pr',
                      default='pr.txt', help='save page rank scores to P')
    parser.add_option('--hits-hubs', metavar='H', dest='hubs',
                      default='hubs.txt', help='save hubs scores to H')
    parser.add_option('--hits-auth', metavar='A', dest='auth',
                      default='auth.txt', help='save authority scores to A')
    parser.add_option('--visualization', metavar='V', dest='image',
                      default='subgraph.png', help='save visualization to V')
    opts, _ = parser.parse_args()
    main(opts.graph, opts.roles, opts.fulltext, opts.names,
         opts.hubs, opts.auth, opts.pr, opts.image)
