import math
import unittest
import graph


A = 'a'
B = 'b'
C = 'c'
D = 'd'
E = 'e'


class GraphTests(unittest.TestCase):
    def test_max_weight_tree(self):
        d = {A: [B, C], B: [C, D], C: [A, B, D], D: [B, C, E], E: []}
        weights = {A: 4, B: 3, C: 5, D: 2, E: 1}
        tostr = lambda r: ''.join(n.payload for n in r)
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            mwt_A = testgraph.max_node_weight_tree(A, weights,  3)
            mwt_B = testgraph.max_node_weight_tree(B, weights, -1)
            mwt_C = testgraph.max_node_weight_tree(C, weights,  3)
            mwt_D = testgraph.max_node_weight_tree(D, weights,  2)
            mwt_E = testgraph.max_node_weight_tree(E, weights, -1)
            self.assertEquals('acb', tostr(mwt_A))
            self.assertEquals('bcade', tostr(mwt_B))
            self.assertEquals('cab', tostr(mwt_C))
            self.assertEquals('dc', tostr(mwt_D))
            self.assertEquals('e', tostr(mwt_E))

    def test_hubs_and_authorities_with_duplicate_edges(self):
        d = {A: [B, B, C], B: [], C: []}
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            hubs, auths = testgraph.hubs_and_authorities(num_iterations=2)
            self.assertEquals(hubs[A], 1)
            self.assertEquals(hubs[B], 0)
            self.assertEquals(hubs[C], 0)
            self.assertEquals(auths[A], 0)
            self.assertEquals(auths[B], 2 / math.sqrt(5))
            self.assertEquals(auths[C], 1 / math.sqrt(5))

    def test_hubs_and_authorities_without_duplicate_edges(self):
        d = {A: [B, C, D], B: [C, D], D: [B]}
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            hubs, auths = testgraph.hubs_and_authorities(num_iterations=19)
            self.assertEquals('0.7887', '%.4f' % hubs[A])
            self.assertEquals('0.0000', '%.4f' % auths[A])
            self.assertEquals('0.5774', '%.4f' % hubs[B])
            self.assertEquals('0.4597', '%.4f' % auths[B])
            self.assertEquals('0.0000', '%.4f' % hubs[C])
            self.assertEquals('0.6280', '%.4f' % auths[C])
            self.assertEquals('0.2113', '%.4f' % hubs[D])
            self.assertEquals('0.6280', '%.4f' % auths[D])

    def test_page_rank_triangle(self):
        d = {A: [B], B: [C], C: [A]}
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            pr = testgraph.page_rank(0.7, num_iterations=1)
            self.assertEquals('0.33', '%.2f' % pr[A])
            self.assertEquals('0.33', '%.2f' % pr[B])
            self.assertEquals('0.33', '%.2f' % pr[C])

    def test_page_rank_triangle_one_repeated(self):
        d = {A: [B], B: [C], C: [A, B]}
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            pr = testgraph.page_rank(0.7, num_iterations=64)
            self.assertEquals('0.2314', '%.4f' % pr[A])
            self.assertEquals('0.3933', '%.4f' % pr[B])
            self.assertEquals('0.3753', '%.4f' % pr[C])

    def test_page_rank_hub_in_the_middle(self):
        d = {A: [B], B: [A, C], C: [B]}
        for is_sparse in [True, False]:
            testgraph = graph.Graph.from_adjacency_list(d, is_sparse=is_sparse)
            pr = testgraph.page_rank(0.7, num_iterations=33)
            self.assertEquals('0.2647', '%.4f' % pr[A])
            self.assertEquals('0.4706', '%.4f' % pr[B])
            self.assertEquals('0.2647', '%.4f' % pr[C])


def main():
    unittest.main()


if __name__ == '__main__':
    main()
