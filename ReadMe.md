# Text Technologies Assessment 4

The year is 2001 and you are helping the US Securities and Exchange Commission
investigate the scandal surrounding the Enron Corporation. The SEC has secured
access to all communications of the relevant persons, but they are having a hard
time understanding the flow of information within the company. Your goal is to
analyse the pattern of email communications and discover who emails whom and
why.

You decide to treat employees as nodes in a social graph and emails as links
between them. Any time person A sends an email to person B, you will interpret
this as a directed link from A to B. Emails that have more than one recipient
should count as several directed links, one for each recipient. For example, if
person A sends an email to persons B and C, you have two directed links: A->B
and A->C. Repeated links should count multiple times. In other words, if A
emails B ten times, you should treat A->B as ten identical links (or as a link
with a weight of 10). Links of the form A->A (i.e. emails sent to oneself)
should be excluded from the graph.

## Dataset

Each email in the email graph (graph.txt) is represented by one or more
sender-recipient lines. Each line contains three fields: message id, sender and
recipient. For example:

    0001049b71b185c9d9605b9eeb1c44f6 vince.kaminski@enron.com mark.palmer@enron.com
    0001049b71b185c9d9605b9eeb1c44f6 vince.kaminski@enron.com vince.kaminski@enron.com
    0001103de1123d6dd538aba7eabd6580 drew.fossum@enron.com martha.benner@enron.com

The above example contains two emails. The first
(id:0001049b71b185c9d9605b9eeb1c44f6) was sent by Vince Kaminski and had two
recipients: Mark Palmer and Vince Kaminski himself. The second email
(id:0001103de1123d6dd538aba7eabd6580) was sent by Drew Fossum to Martha Benner.
The file contains a total 1,356,626 lines, corresponding to 242,047 distinct
emails from 19,802 distinct senders to 77,660 distinct recipients. Some email
addresses are malformed but you should include them as nodes in the graph.

## Tasts

### 1. Run PageRank and Hits

Implement the PageRank and the Hubs and Authorities algorithms.  For PageRank
set lambda=0.8. Run both algorithms for 10 iterations on the above graph. Save
the results in three files:

- hubs.txt: 10 persons with the highest hub score. Each line should contain an
  email address, preceded by its hub score (show 8 decimal digits), for example:
  `0.00100596 jeff.dasovich@enron.com`
- auth.txt: 10 persons with the highest authority score (same format as hubs).
- pr.txt: 10 persons with the highest PageRank score (same format as hubs).

### 2. Visualize Key Connections

Select 10-20 key persons and visualize the flow of information among them using
Graphviz. Create a file called graph.dot in the following format:

    digraph G {
       "kate.symes" -> "tana.jones" ;
       "kate.symes" -> "kysa.alport" [label = "trading"];
       "kysa.alport" -> "kate.symes" ;
    }

Then use the following command on DICE: `dot -Tpng graph.dot > graph.png` If you
don't like the layout, try `man dot`.  It is up to you how you select the key
persons, which connections you include and what labels (if any) you assign to
the connections.  You may find the following resources helpful:

- A partial roster of Enron employees (roles.txt)
- The complete text of all emails (enron.xml.bz2)
